<?php
namespace Molla\Vesicash\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        
        $setup->getConnection()->addColumn(
            $setup->getTable('quote_payment'),
            'customer_email',
            [
                'type' => 'text',
                'nullable' => true  ,
                'comment' => 'vesicash Email',
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_payment'),
            'customer_email',
            [
                'type' => 'text',
                'nullable' => true  ,
                'comment' => 'vesicash Email',
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('quote_payment'),
            'vesicash_status',
            [
                'type' => 'text',
                'nullable' => true  ,
                'comment' => 'vesicash Status',
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_payment'),
            'vesicash_status',
            [
                'type' => 'text',
                'nullable' => true  ,
                'comment' => 'vesicash Status',
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('quote_payment'),
            'vesicash_url',
            [
                'type' => 'text',
                'nullable' => true  ,
                'comment' => 'vesicash Url',
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_payment'),
            'vesicash_url',
            [
                'type' => 'text',
                'nullable' => true  ,
                'comment' => 'vesicash Url',
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('quote_payment'),
            'vesicash_tranid',
            [
                'type' => 'text',
                'nullable' => true  ,
                'comment' => 'Transaction ID',
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_payment'),
            'vesicash_tranid',
            [
                'type' => 'text',
                'nullable' => true  ,
                'comment' => 'Transaction ID',
            ]
        );
        $setup->endSetup();
    }
}
<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_CsMarketplace
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Molla\Vesicash\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;


/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * CsMarketplace setup factory
     *
     * @var CsMarketplaceSetupFactory
     */
    private $csmarketplaceSetupFactory;

    public $_objectManager;

    /**
     * InstallData constructor.
     * @param CsMarketplaceSetupFactory $csmarketplaceSetupFactory
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(EavSetupFactory $csmarketplaceSetupFactory, \Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->csmarketplaceSetupFactory = $csmarketplaceSetupFactory;
        $this->_objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        /**
         *
         *
         * @var CsMarketplaceSetup $csmarketplaceSetup
         */
        $csmarketplaceSetup = $this->csmarketplaceSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        $csmarketplaceSetup->installEntities();
       
        $csmarketplaceSetup->addAttribute(
            'csmarketplace_vendor', 'vesi_account_id', array(
                'group' => 'General Information',
                'visible' => true,
                'position' => 200,
                'type' => 'varchar',
                'label' => 'Vesicash Account Id',
                'input' => 'text',
                'required' => false,
                'user_defined' => true
            )
        );

        

        $csmarketplaceSetup->addAttribute(
            'csmarketplace_vendor', 'vesi_account_name', array(
                'group' => 'General Information',
                'visible' => true,
                'position' => 208,
                'type' => 'varchar',
                'label' => 'Vesicash Account Name',
                'input' => 'text',
                'required' => false,
                'user_defined' => true,
                'frontend_class' => 'validate-no-html-tags'
            )
        );


        $csmarketplaceSetup->addAttribute(
            'csmarketplace_vendor', 'vesi_account_no', array(
                'group' => 'General Information',
                'visible' => true,
                'position' => 210,
                'type' => 'varchar',
                'label' => 'Vesicash Account NO',
                'input' => 'text',
                'required' => false,
                'user_defined' => true,
                'frontend_class' => 'validate-no-html-tags'
            )
        );

        $csmarketplaceSetup->addAttribute(
            'csmarketplace_vendor', 'vesi_bank_id', array(
                'group' => 'General Information',
                'visible' => true,
                'position' => 220,
                'type' => 'varchar',
                'label' => 'Vesicash Bank ID',
                'input' => 'select',
                'source' => 'Molla\Customer\Model\Config\Source\Bankid',
                'default_value' => 'general',
                'required' => false,
                'user_defined' => true,
                'note' => ''
            )
        );
        $csmarketplaceSetup->addAttribute(
            'csmarketplace_vendor', 'vesi_mobile_money_operator', array(
                'group' => 'General Information',
                'visible' => true,
                'position' => 220,
                'type' => 'varchar',
                'label'        => 'Mobile Money Operator for Vesicash',
				'input'        => 'select',
				"source"   => "Molla\Customer\Model\Config\Source\Mobile",
				'required'     => false,
                'user_defined' => true,
                'note' => ''
            )
        );
     

       

       

       

        $profileDisplayAttributes = array(
            'public_name' => array('sort_order' => 10, 'fontawesome' => 'fa fa-user', 'store_label' => 'Public Name'),
            'support_number' => array('sort_order' => 20, 'fontawesome' => 'fa fa-mobile', 'store_label' => 'Tel'),
            'support_email' => array('sort_order' => 30, 'fontawesome' => 'fa fa-envelope-o', 'store_label' => 'Support Email'),
            'email' => array('sort_order' => 35, 'fontawesome' => 'fa fa-envelope-o', 'store_label' => 'Email'),
            'company_name' => array('sort_order' => 40, 'fontawesome' => 'fa fa-building', 'store_label' => 'Company'),
            'name' => array('sort_order' => 50, 'fontawesome' => 'fa fa-user', 'store_label' => 'Representative'),
            'company_address' => array('sort_order' => 60, 'fontawesome' => 'fa fa-location-arrow', 'store_label' => 'Location'),
            'created_at' => array('sort_order' => 70, 'fontawesome' => 'fa fa-calendar', 'store_label' => 'Vendor Since'),
            'facebook_id' => array('sort_order' => 80, 'fontawesome' => 'fa fa-facebook-square', 'store_label' => 'Find us on Facebook'),
            'twitter_id' => array('sort_order' => 90, 'fontawesome' => 'fa fa-twitter', 'store_label' => 'Follow us on Twitter')
        );
        $vendorAttributes = $this->_objectManager->get('Ced\CsMarketplace\Model\Vendor\Form')->getCollection();
        if (count($vendorAttributes) > 0) {
            $storesViews = $this->_objectManager->get('Magento\Store\Model\Store')->getCollection();
            foreach ($vendorAttributes as $vendorAttribute) {
                $vendorMainAttribute = $this->_objectManager->get('Ced\CsMarketplace\Model\Vendor\Form')
                                        ->load($vendorAttribute->getAttributeId());
                $isSaveNeeded = false;
                if (isset($registrationFormAttributes[$vendorAttribute->getAttributeCode()])) {
                    $vendorAttribute->setData('use_in_registration', 1);
                    $vendorAttribute->setData('position_in_registration', $registrationFormAttributes[$vendorAttribute->getAttributeCode()]);
                    $isSaveNeeded = true;
                }
                if (isset($profileDisplayAttributes[$vendorAttribute->getAttributeCode()])) {
                    $frontend_label[0] = $vendorMainAttribute->getFrontendLabel();
                    foreach ($storesViews as $storesView) {
                        $frontend_label[$storesView->getId()] = $profileDisplayAttributes[$vendorAttribute->getAttributeCode()]['store_label'];
                    }
                    $vendorAttribute->setData('use_in_left_profile', 1);
                    $vendorAttribute->setData('position_in_left_profile', $profileDisplayAttributes[$vendorAttribute->getAttributeCode()]['sort_order']);
                    $vendorAttribute->setData('fontawesome_class_for_left_profile', $profileDisplayAttributes[$vendorAttribute->getAttributeCode()]['fontawesome']);
                    $vendorMainAttribute->setData('frontend_label', $frontend_label);
                    $isSaveNeeded = true;
                }
                if ($isSaveNeeded) {
                    $vendorAttribute->save();
                    $vendorMainAttribute->save();
                    $isSaveNeeded = false;
                }
            }
        }
        $setup->endSetup();
    }
}

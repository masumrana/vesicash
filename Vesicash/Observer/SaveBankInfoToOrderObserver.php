<?php
namespace Molla\Vesicash\Observer;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Molla\Vesicash\Helper\Data;
class SaveBankInfoToOrderObserver implements ObserverInterface {
    protected $_inputParamsResolver;
    protected $_quoteRepository;
    protected $logger;
    protected $_state;
    public function __construct(
        \Magento\Webapi\Controller\Rest\InputParamsResolver $inputParamsResolver,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\State $state,
        \Molla\Vesicash\Helper\Data $escrohelper
        ) {
        $this->_inputParamsResolver = $inputParamsResolver;
        $this->_quoteRepository = $quoteRepository;
        $this->logger = $logger;
        $this->_state = $state;
        $this->escroeHelper = $escrohelper;
    }

    public function execute(EventObserver $observer) {
        $inputParams = $this->_inputParamsResolver->resolve();
        if($this->_state->getAreaCode() != \Magento\Framework\App\Area::AREA_ADMINHTML){
        foreach ($inputParams as $inputParam) {
            if ($inputParam instanceof \Magento\Quote\Model\Quote\Payment) {
                $paymentData = $inputParam->getData('additional_data');
                $paymentOrder = $observer->getEvent()->getPayment();
                $order = $paymentOrder->getOrder();
                $quote = $this->_quoteRepository->get($order->getQuoteId());
                $paymentQuote = $quote->getPayment();
                $method = $paymentQuote->getMethodInstance()->getCode();
                if ($method == 'vesicash') {
                    if($paymentData['account_id']){
                        $result = $this->escroeHelper->veshipayment($paymentData['account_id'], $quote->getAllVisibleItems(), $order->getShippingAmount());
                        $paymentQuote->setData('vesicash_tranid', $result['tid']);
                        $paymentOrder->setData('vesicash_tranid', $result['tid']);
                        $paymentQuote->setData('vesicash_url', $result['buyeraccountid']);
                        $paymentOrder->setData('vesicash_url', $result['buyeraccountid']);
                    }
                    else {
                        $result = $this->escroeHelper->paymentWithoutId($order->getBillingAddress(), $quote->getAllVisibleItems(), $order->getShippingAmount());
                        $paymentQuote->setData('vesicash_tranid', $result['tid']);
                        $paymentOrder->setData('vesicash_tranid', $result['tid']);
                        $paymentQuote->setData('vesicash_url', $result['buyeraccountid']);
                        $paymentOrder->setData('vesicash_url', $result['buyeraccountid']);
                    }
                   
                }
                
            }
        }
       }
    }
}

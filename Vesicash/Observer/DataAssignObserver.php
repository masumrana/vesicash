<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * OfflinePayments Observer
 */
namespace Molla\Vesicash\Observer;

use Magento\Framework\DataObject;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Payment\Model\InfoInterface;


class DataAssignObserver extends AbstractDataAssignObserver
{
    /**
     * Sets current instructions for bank transfer account
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        
        $data = $this->readDataArgument($observer);
        
        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        //print_r($additionalData);
        $additionalData = new DataObject($additionalData);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('\Psr\Log\LoggerInterface');
        /* save log into debug.log */
        $logger->debug('MASUM'.json_encode($additionalData));
        $logger->info('MASUM'.json_encode($additionalData));
        if (!is_array($additionalData)) {
            return;
        }
        $additionalData = new DataObject($additionalData);
        $paymentMethod = $this->readMethodArgument($observer);
        $payment = $observer->getPaymentModel();
        if (!$payment instanceof InfoInterface) {
            $payment = $paymentMethod->getInfoInstance();
        }
        if (!$payment instanceof InfoInterface) {
            throw new LocalizedException(__('Payment model does not provided.'));
        }
		$payment->setAdditionalInformation(
                'customer_email','ranamasum2@gmail.com'
        );
		$payment->setAdditionalInformation(
                'payment_status','Pending'
        );
    }
}

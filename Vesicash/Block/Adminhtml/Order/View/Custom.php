<?php
namespace Molla\Vesicash\Block\Adminhtml\Order\View;

class Custom extends \Magento\Sales\Block\Adminhtml\Order\View {

	
	public function getOrderDetails(){
		$order = $this->getOrder();
		$payment = $order->getPayment();
		return $payment;
	}
	
}
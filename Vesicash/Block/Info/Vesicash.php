<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Molla\Vesicash\Block\Info;

class Vesicash extends \Magento\Payment\Block\Info
{
    /**
     * @var string
     */
    protected $_template = 'Molla_Vesicash::info/vesicash.phtml';

    /**
     * @return string
     */
    public function toPdf()
    {
        $this->setTemplate('Molla_Vesicash::info/pdf/vesicash.phtml');
        return $this->toHtml();
    }
}

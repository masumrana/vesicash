<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Molla\Vesicash\Block\Form;

/**
 * Block for Debit Payment payment method form
 */
class Vesicash extends \Molla\Vesicash\Block\Form\AbstractInstruction
{
    /**
     * Debit Payment Template
     *
     * @var string
     */
    protected $_template = 'form/vesicash.phtml';

}

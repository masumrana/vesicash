<?php 

namespace Molla\Vesicash\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Api implements ArrayInterface
{

	/*
	 * Option getter
	 * @return array
	 */
	public function toOptionArray()
	{
	    $arr = $this->toArray();
	    $ret = [];
	    foreach ($arr as $key => $value) {
		$ret[] = [
		    'value' => $key,
		    'label' => $value
		];
	    }
	    return $ret;
	}

	/*
	 * Get options in "key-value" format
	 * @return array
	 */
	public function toArray()
	{
	    $choose = [
			'https://api.vesicash.com/v1/' => 'Live',
			'https://sandbox.api.vesicash.com/v1/' => 'Sandbox',
	    ];
	    return $choose;
	}

}

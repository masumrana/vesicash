<?php 

namespace Molla\Vesicash\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Transaction implements ArrayInterface
{

	/*
	 * Option getter
	 * @return array
	 */
	public function toOptionArray()
	{
	    $arr = $this->toArray();
	    $ret = [];
	    foreach ($arr as $key => $value) {
		$ret[] = [
		    'value' => $key,
		    'label' => $value
		];
	    }
	    return $ret;
	}

	/*
	 * Get options in "key-value" format
	 * @return array
	 */
	public function toArray()
	{
	    $choose = [
			'product' => 'Product',
	    ];
	    return $choose;
	}

}

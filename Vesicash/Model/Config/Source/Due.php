<?php 

namespace Molla\Vesicash\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Due implements ArrayInterface
{

	/*
	 * Option getter
	 * @return array
	 */
	public function toOptionArray()
	{
	    $arr = $this->toArray();
	    $ret = [];
	    foreach ($arr as $key => $value) {
		$ret[] = [
		    'value' => $key,
		    'label' => $value
		];
	    }
	    return $ret;
	}

	/*
	 * Get options in "key-value" format
	 * @return array
	 */
	public function toArray()
	{
	    $choose = [
			'1 day'   => '1 day',
			'2 days'  => '2 days',
			'5 days'  => '5 days',
			'7 days'  => '7 days',
			'10 days' => '10 days',
			'15 days' => '15 days',
			'20 days' => '20 days',
			'30 days' => '30 days',
	    ];
	    return $choose;
	}

}

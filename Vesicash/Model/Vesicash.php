<?php
 
namespace Molla\Vesicash\Model;
 
/**
 * Pay In Store payment method model
 */
class Vesicash extends \Magento\Payment\Model\Method\AbstractMethod
{
    /**
     * Payment method code
     *
     * @var string
     */
	const PAYMENT_METHOD_Vesicash_CODE = 'vesicash';
    protected $_code = self::PAYMENT_METHOD_Vesicash_CODE;
	protected $_infoBlockType = \Molla\Vesicash\Block\Info\Vesicash::class;
	protected $_formBlockType = \Molla\Vesicash\Block\Form\Vesicash::class;
	
	public function getInstructions()
    {
        return trim($this->getConfigData('instructions'));
    }
}
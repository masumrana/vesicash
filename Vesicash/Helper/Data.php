<?php
 
namespace Molla\Vesicash\Helper;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
 
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_sellerEmail;
    protected $_customerFactory;
    protected $_addressFactory;
 
    public function __construct(
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory
    )
    {
        $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory;
    }
	
	
	public function getSystemValue($path){
		$_objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$scopeConfig = $_objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
		$configPath = 'payment/vesicash/'.$path;
		return $value =  $scopeConfig->getValue(
		    $configPath,
		    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
		);
    }
    
    public function getCheckoutUrl($tid){
        if ($this->getSystemValue('url') == 'https://api.vesicash.com/v1/') {
            return 'https://vesicash.com/checkout/'. $tid;
        }
        if ($this->getSystemValue('url') == 'https://sandbox.api.vesicash.com/v1/') {
            return 'https://sandbox.vesicash.com/checkout/'. $tid;
        }
    }

	public function getSellerEmail($item){
		$model =  \Magento\Framework\App\ObjectManager::getInstance();
		$productSku = $item->getProduct()->getSku();
		$VendorProduct = $model->create('Ced\CsMarketplace\Model\Vproducts')->getCollection()
				->addFieldToFilter('sku',$productSku)->addFieldToFilter('check_status',['nin'=>3])->getFirstItem();
		$VendorId = $VendorProduct->getVendorId();
		if (!empty($VendorId)) {
			$array = array('name');
			$vendorModel = $model->create('Ced\CsMarketplace\Model\Vendor')->getCollection()->addAttributeToSelect(array('*'))->addAttributeToFilter('entity_id',$VendorId)->toArray();
			$vendor = $model->create('Ced\CsMarketplace\Model\Vendor')->load($VendorId);
			return $vendor;
		}
    }	
    public function paymentWithoutId($billingaddress, $items, $shippingAmount){
        $accountId = $this->createUser($billingaddress);
        return $this->veshipayment($accountId, $items, $shippingAmount);
        
    }

    public function veshipayment($accountId, $items, $shippingAmount){
        $key = $this->getSystemValue('v_private_key');
        $url = $this->getSystemValue('url')."transactions/create";
        $product_title = 'Vesicash Payment In Oneagrix';
        $item_array = [];
        foreach($items as $item) {
            $sellerdata = $this->getSellerEmail($item);
            $sellerID = $this->createSeller($sellerdata);
            array_push($item_array, array(
                'quantity' => (int) $item->getQty(),
                'amount'    => (float) $item->getPrice()*$item->getQty(),
                'title' => $item->getName(),
                )
            );
        }
        if ($this->getSystemValue('escrow_charge_bearer') == 'buyer') {
            $charge_bearer = $accountId;
        }
        if ($this->getSystemValue('escrow_charge_bearer') == 'seller') {
            $charge_bearer = (int) $sellerID;
        }

        $parties = array(
            "buyer" => (int) $accountId,
            "sender" => (int) $sellerID,
            "seller" => (int) $sellerID,
            "recipient" => (int) $accountId,
            "charge_bearer" => (int) $charge_bearer
        );
         $today = date('m/d/Y');
         $number_of_days = $this->getSystemValue('due_date');
         $new_due_date = date('m/d/Y', strtotime("$today + $number_of_days"));
         $request = array(
            'title' => $product_title,
            'description' => $product_title,
            'type' => $this->getSystemValue('transaction_type'),
            'products' => $item_array,
            'parties' => $parties,
            'currency' => $this->getSystemValue('currency'),
            'inspection_period' => (int) $this->getSystemValue('inspection_period'),
            'shipping_fee' => (int) $shippingAmount,
            'due_date' => $new_due_date
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($request),
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "V-PRIVATE-KEY: $key"
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $body = json_decode($response);
        if($body->status == "ok" ) {
            $data = array('tid'=>$body->data->transaction->transaction_id, 'buyeraccountid'=> $accountId );
            return $data;
        }
        return false;
    }

    public function createSeller($data){
        $url = $this->getSystemValue('url').'auth/signup'; 
        $key = $this->getSystemValue('v_private_key');
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL =>  $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode(array('firstname'=> $data->getPublicName(), 'lastname'=> $data->getName(), 'country' => $data->getCountryId(),'email_address' => $data->getEmail())),
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "V-PRIVATE-KEY: $key"
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $reg_buyer = json_decode($response);
        if($reg_buyer->status == "ok" ) {
            return $buyer_id = $reg_buyer->data->user->account_id;
        }
        return false;
    } 

    public function createUser($billingaddress){
        $url = $this->getSystemValue('url').'auth/signup'; 
        $key = $this->getSystemValue('v_private_key');
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL =>  $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode(array('firstname'=> $billingaddress->getFirstName(), 'lastname'=> $billingaddress->getLastName(), 'country' => $billingaddress->getCountryId(),'email_address' => $billingaddress->getEmail(),'phone_number' => $billingaddress->getTelephone())),
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "V-PRIVATE-KEY: $key"
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $reg_buyer = json_decode($response);
        if($reg_buyer->status == "ok" ) {
            return $buyer_id = $reg_buyer->data->user->account_id;
        }
        return false;
    }
    public function addsuppilerBank($data,$id){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $objectManager->create('Ced\CsMarketplace\Model\Vendor');
        $model->load($id);
        $accountId = $this->createSeller($model);
        $adata['account_id']=  $accountId;
        $adata['vesicash_bank_id']=  $data['vesi_bank_id'];
        $adata['vesicash_account_name']=  $data['vesi_account_name'];
        $adata['vesicash_account_no']=  $data['vesi_account_no'];
        $adata['mobile_money_operator']=  $data['vesi_mobile_money_operator'];
        $this->addvesibank($adata);
        return $accountId;
    }


    public function addBank($data){
        if(isset($data['account_vesi']) && $data['account_vesi'] == 1){
            $customer = $this->_customerFactory->create()->load($data['customer_id']); 
            $data['account_id'] = $customer->getVesicashAccountId();
            
            if($this->addvesibank($data)){
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $customerRepository = $objectManager->create('Magento\Customer\Api\CustomerRepositoryInterface');
                $customer = $customerRepository->getById($data['customer_id']);
                //$customer->setCustomAttribute('vesicash_account_id', $data['account_id']);
                $customer->setCustomAttribute('vesicash_account_id',$data['account_id']);
                $customer->setCustomAttribute('vesicash_bank_id',$data['vesicash_bank_id']);
                $customer->setCustomAttribute('vesicash_account_name',$data['vesicash_account_name']);
                $customer->setCustomAttribute('vesicash_account_no',$data['vesicash_account_no']);
                $customer->setCustomAttribute('mobile_money_operator',$data['mobile_money_operator']);
                $customerRepository->save($customer);
                return true;
            }
        }
        else{
            $customer = $this->_customerFactory->create()->load($data['customer_id']); 
            $billingAddressId = $customer->getDefaultBilling();
            $billingAddress = $this->_addressFactory->create()->load($billingAddressId);
            //echo $this->createCustomerUser($billingAddress);
            $data['account_id'] = $this->createCustomerUser($billingAddress);
            if($this->addvesibank($data)){
                
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $customerRepository = $objectManager->create('Magento\Customer\Api\CustomerRepositoryInterface');
                $customer = $customerRepository->getById($data['customer_id']);
                //$customer->setCustomAttribute('vesicash_account_id', $data['account_id']);
                $customer->setCustomAttribute('vesicash_account_id',$data['account_id']);
                $customer->setCustomAttribute('vesicash_bank_id',$data['vesicash_bank_id']);
                $customer->setCustomAttribute('vesicash_account_name',$data['vesicash_account_name']);
                $customer->setCustomAttribute('vesicash_account_no',$data['vesicash_account_no']);
                $customer->setCustomAttribute('mobile_money_operator',$data['mobile_money_operator']);
                $customerRepository->save($customer);
                return true;
            }
            return false;
        }
    }

    public function createCustomerUser($billingaddress){
        $url = $this->getSystemValue('url').'auth/signup'; 
        $key = $this->getSystemValue('v_private_key');
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL =>  $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode(array('firstname'=> $billingaddress->getFirstName(), 'lastname'=> $billingaddress->getLastName(), 'country' => $billingaddress->getCountryId(),'email_address' => $billingaddress->getEmail(),'phone_number' => $billingaddress->getTelephone())),
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "V-PRIVATE-KEY: $key"
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $reg_buyer = json_decode($response);
        if($reg_buyer->status == "ok" ) {
            return $buyer_id = $reg_buyer->data->user->account_id;
        }
        return false;
    }




    public function addvesibank($data){
        $url = $this->getSystemValue('url').'admin/user/update/bank'; 
        $key = $this->getSystemValue('v_private_key');
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL =>  $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode(
            array(
                'account_id'=> $data['account_id'],
                'updates'=> array(
                    'bank_id'=> $data['vesicash_bank_id'],
                    'account_name'=> $data['vesicash_account_name'],
                    'account_no'=> $data['vesicash_account_no'],
                    'mobile_money_operator'=> $data['mobile_money_operator']
                ) 
            )
        ),
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "V-PRIVATE-KEY: $key"
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $reg_buyer = json_decode($response);
        if($reg_buyer->status == "ok" ) {
            return true;
        }
        return false;
    } 
    
}

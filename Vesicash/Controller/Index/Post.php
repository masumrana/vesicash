<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Molla\Vesicash\Controller\Index;


use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;
use Molla\Vesicash\Helper\Data;

class Post extends \Magento\Framework\App\Action\Action 
{

    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor
    ) {
        parent::__construct($context);
        $this->context = $context;
        $this->dataPersistor = $dataPersistor;
    }

    /**
     * Post user question
     *
     * @return Redirect
     */
    public function execute()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }
        try {
            $helper = \Magento\Framework\App\ObjectManager::getInstance()->get('Molla\Vesicash\Helper\Data');
            $helper->addBank($this->getRequest()->getPostValue());
            $this->messageManager->addSuccessMessage(
                __('Thanks for Adding Bank Details.')
            );
            $this->dataPersistor->clear('vesi_account');
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->dataPersistor->set('vesi_account', $this->getRequest()->getParams());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('An error occurred while processing your form. Please try again later.')
            );
            $this->dataPersistor->set('vesi_account', $this->getRequest()->getParams());
        }
        return $this->resultRedirectFactory->create()->setPath('vesicash/index');
    }
}

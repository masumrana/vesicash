<?php
namespace Molla\Vesicash\Controller\Index;

use Magento\Framework\App\ObjectManager;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
		$objectManager = ObjectManager::getInstance();
 		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
    	if($customerSession->isLoggedIn()) {
			$this->_view->loadLayout(); 
			$this->_view->renderLayout(); 
		}
		else{
			return $this->resultRedirectFactory->create()->setPath('customer/account/');
		}
	}
}

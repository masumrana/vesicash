define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'vesicash',
                component: 'Molla_Vesicash/js/view/payment/method-renderer/vesicash-method'
            }
        );
        return Component.extend({});
    }
);
<?php

namespace Molla\Customer\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

class InstallData implements InstallDataInterface
{
	private $eavSetupFactory;

	public function __construct(
		EavSetupFactory $eavSetupFactory,
		Config $eavConfig,
		AttributeSetFactory $attributeSetFactory
	)
	{
		$this->eavSetupFactory = $eavSetupFactory;
		$this->eavConfig       = $eavConfig;
		$this->attributeSetFactory = $attributeSetFactory;
	}

	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		$customerEntity = $this->eavConfig->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
		$attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
		
		$eavSetup->addAttribute(
			\Magento\Customer\Model\Customer::ENTITY,
			'vesicash_account_id',
			[
				'type'         => 'varchar',
				'label'        => 'Vesicash Account Id',
				'input'        => 'text',
				'required'     => false,
				'visible'      => false,
				'user_defined' => true,
				'position'     => 1002,
				'system'       => 0,
			]
		);
		$vesicash_account_id = $this->eavConfig->getAttribute(Customer::ENTITY, 'vesicash_account_id');
		$vesicash_account_id->addData([
			'attribute_set_id' => $attributeSetId,
			'attribute_group_id' => $attributeGroupId,
			'used_in_forms' => ['adminhtml_customer'],
		]);
		$vesicash_account_id->save();

		$eavSetup->addAttribute(
			\Magento\Customer\Model\Customer::ENTITY,
			'vesicash_account_name',
			[
				'type'         => 'varchar',
				'label'        => 'Vesicash Account Name',
				'input'        => 'text',
				'required'     => false,
				'visible'      => true,
				'user_defined' => true,
				'position'     => 1002,
				'system'       => 0,
			]
		);
		$vesicash_account_name = $this->eavConfig->getAttribute(Customer::ENTITY, 'vesicash_account_name');
		$vesicash_account_name->addData([
			'attribute_set_id' => $attributeSetId,
			'attribute_group_id' => $attributeGroupId,
			'used_in_forms' => ['adminhtml_customer','customer_account_create','customer_account_edit'],
		]);
		$vesicash_account_name->save();

		$eavSetup->addAttribute(
			\Magento\Customer\Model\Customer::ENTITY,
			'vesicash_account_no',
			[
				'type'         => 'varchar',
				'label'        => 'Vesicash Account No',
				'input'        => 'text',
				'required'     => false,
				'visible'      => true,
				'user_defined' => true,
				'position'     => 1002,
				'system'       => 0,
			]
		);
		$vesicash_account_no = $this->eavConfig->getAttribute(Customer::ENTITY, 'vesicash_account_no');
		$vesicash_account_no->addData([
			'attribute_set_id' => $attributeSetId,
			'attribute_group_id' => $attributeGroupId,
			'used_in_forms' => ['adminhtml_customer','customer_account_create','customer_account_edit'],
		]);
		$vesicash_account_no->save();

		$eavSetup->addAttribute(
			\Magento\Customer\Model\Customer::ENTITY,
			'vesicash_bank_id',
			[
				'type'         => 'int',
				'label'        => 'Select Bank Id for Vesicash',
				'input'        => 'select',
				"source"   => "Molla\Customer\Model\Config\Source\Bankid",
				'required'     => false,
				'visible'      => true,
				'user_defined' => true,
				'position'     => 1004,
				'system'       => 0,
			]
		);
		$vesicash_bank_id = $this->eavConfig->getAttribute(Customer::ENTITY, 'vesicash_bank_id');
		$vesicash_bank_id->addData([
			'attribute_set_id' => $attributeSetId,
			'attribute_group_id' => $attributeGroupId,
			'used_in_forms' => ['adminhtml_customer','customer_account_create','customer_account_edit'],
		]);
		$vesicash_bank_id->save();

		$eavSetup->addAttribute(
			\Magento\Customer\Model\Customer::ENTITY,
			'mobile_money_operator',
			[
				'type'         => 'int',
				'label'        => 'Mobile Money Operator for Vesicash',
				'input'        => 'select',
				"source"   => "Molla\Customer\Model\Config\Source\Mobile",
				'required'     => false,
				'visible'      => true,
				'user_defined' => true,
				'position'     => 1005,
				'system'       => 0,
			]
		);
		$mobile_money_operator = $this->eavConfig->getAttribute(Customer::ENTITY, 'mobile_money_operator');
		$mobile_money_operator->addData([
			'attribute_set_id' => $attributeSetId,
			'attribute_group_id' => $attributeGroupId,
			'used_in_forms' => ['adminhtml_customer','customer_account_create','customer_account_edit'],
		]);
		$mobile_money_operator->save();
	}
}
